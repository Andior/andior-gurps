# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.3.4

### Added

- Character notes

## 0.3.3

### Added

- Power Tally tracking for campaigns with Threshold magic.
- Add points and unspent points tracking
- Show points on Advanatages page

### Changed

- Localized actor-sheet
- CSS for flex-group - removed border
- Split skill level to two fields - level and points.

### Fixed

- Item list header is no longer draggable.

## 0.3.2

### Added

- Enabled built-in drag & drop sorting of items.

### Fixed

- Skill level had remove CSS class `skill-level` instead of generic `item-level`

## 0.3.1

### Changed

- Add basic set advantages imported from GCS.
- Advantages and Skills are sorted based on their point value.

## 0.3.0

### Added

- Added a changelog
- Added support for initiative tracking based on Basic Speed.
- Added support for advantages/disadvantages

### Removed

- Support for version<0.7.0
- Unused function `AGURPSItem.rollSkill`

## 0.2.6

Changelog was not kept in these versions
