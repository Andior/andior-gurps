/**
 * This is your JavaScript entry file for Foundry VTT.
 * Register custom settings, sheets, and constants using the Foundry API.
 * Change this heading to be more descriptive to your system, or remove it.
 * Author: [your name]
 * Content License: [copyright and-or license] If using an existing system
 * 					you may want to put a (link to a) license or copyright
 * 					notice here (e.g. the OGL).
 * Software License: [your license] Put your desired license here, which
 * 					 determines how others may use and modify your system
 */

// Import JavaScript modules
import { registerSettings } from './module/settings.js';
import { preloadTemplates } from './module/preloadTemplates.js';
import { AGURPSActor } from './module/actor/actor.js';
import { AGURPSActorSheet } from './module/actor/actor-sheet.js';
import { AGURPSItem } from './module/item/item.js';
import { AGURPSItemSheet } from './module/item/item-sheet.js';
import { AGURPS } from './module/config.js';

import { GURPSSkillRollRE, formatRollResult } from './module/util.js';

/* ------------------------------------ */
/* Initialize system					*/
/* ------------------------------------ */
Hooks.once('init', async function() {
    console.log('andior-gurps | Initializing andior-gurps');

    // Assign custom classes and constants here
    CONFIG.Actor.entityClass = AGURPSActor;
    CONFIG.Item.entityClass = AGURPSItem;

    CONFIG.AGURPS = AGURPS;

    game.system.data.initiative = "@attributes.bs.value";

    // Register custom system settings
    registerSettings();
    
    // Preload Handlebars templates
    await preloadTemplates();

    // Register custom sheets (if any)
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("andior-gurps", AGURPSActorSheet, {makeDefault: true});
    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("andior-gurps", AGURPSItemSheet, {makeDefault: true});

});

/* ------------------------------------ */
/* Setup system							*/
/* ------------------------------------ */
Hooks.once('setup', function() {
    // Do anything after initialization but before
    // ready
});

/* ------------------------------------ */
/* When ready							*/
/* ------------------------------------ */
Hooks.once('ready', function() {
    // Do anything once the system is ready
});

Hooks.on('renderChatMessage', function (message, html, data) {
    if ( ! message.isRoll || ! GURPSSkillRollRE.test(message.roll.formula) ) {
        return;
    } else {
        let result = html.find(".dice-total")[0]
        result.innerText = formatRollResult(message.roll.formula, message.roll.result)
    }

})
