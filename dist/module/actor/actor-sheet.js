import { promptForModifier } from '../util.js';

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class AGURPSActorSheet extends ActorSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["andior-gurps", "sheet", "actor"],
            template: "systems/andior-gurps/templates/actor/actor-sheet.html",
            width: 600,
            height: 600,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
        });
    }
    /* -------------------------------------------- */

    /** @override */
    getData() {
        const data = super.getData();
        data.dtypes = ["String", "Number", "Boolean"];
        data.config = CONFIG.AGURPS;
        
        return data;
    }

    /** ------------------------------------------- */

    /** @override */
    
    activateListeners(html) {
        super.activateListeners(html);

        if (!this.options.editable) return;

        // Add Owned Item
        html.find('.item-create').click(this._onItemCreate.bind(this));

        // Edit Owned Item
        html.find(".item-edit").click(ev => {
            ev.preventDefault();
            const li = $(ev.currentTarget).parents(".item");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            item.sheet.render(true);
        })

        // Delete Owned Item
        html.find(".item-delete").click(ev => {
            ev.preventDefault();
            const li = $(ev.currentTarget).parents(".item");
            this.actor.deleteOwnedItem(li.data("itemId"))
            li.slideUp(200, () => this.render(false))
        })

        // Roll Owned Item
        html.find(".item-roll").click(this._onItemRoll.bind(this));

        // Refresh Owned Items
        html.find(".item-refresh").click(ev => {
            ev.preventDefault();
            this.actor.updateItems();
            this.render();
        })

        html.find(".item-reference").click(this._onItemReference.bind(this));

    }

    /* -------------------------------------------- */

    _onItemCreate(event) {
        event.preventDefault();
        const header = event.currentTarget;
        // Get the type of item to create.
        const type = header.dataset.type;
        // Grab any data associated with this control.
        const data = duplicate(header.dataset);
        // Initialize a default name.
        const name = `New ${type.capitalize()}`;
        // Prepare the item object.
        const itemData = {
          name: name,
          type: type,
          data: data
        };
        // Remove the type from the dataset since it's in the itemData.type prop.
        delete itemData.data["type"];
    
        // Finally, create the item!
        return this.actor.createOwnedItem(itemData);
    }

    /* -------------------------------------------- */

    _onItemRoll(event) {
        event.preventDefault();
        const li = $(event.currentTarget).parents(".item");
        const item = this.actor.getOwnedItem(li.data("itemId"));
        item.data.data.defaultRoll();
    }

    /* -------------------------------------------- */

    _onItemReference(event) {
        event.preventDefault()
        const li = $(event.currentTarget).parents(".item");
        const item = this.actor.getOwnedItem(li.data("itemId"));
        item.data.data.openReference();
    }
}
