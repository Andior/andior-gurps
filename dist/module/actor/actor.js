/**
 * Basic Actor entity for the GURPS system
 * @extends {Actor}
 */

export class AGURPSActor extends Actor {
    /**
     * Augment the basic actor data with additional dynamic data.
     * @override
     */
    prepareData() {
        super.prepareData();

        const actorData = this.data;
        const data = actorData.data;
        const flags = actorData.flags;

        // Make separate methods for each Actor type (character, npc, etc.) to keep
        // things organized.
        if (actorData.type === 'character') this._prepareCharacterData(actorData);
    }

    /** ----------------------------------------------- */

    _prepareCharacterData(actorData) {
        const items = actorData.items;

        actorData.skills = items.filter((x) => { return x.type === 'skill'; }).sort((a,b) => (a.sort || 0) - (b.sort || 0));
        actorData.advantages = items.filter((x) => { return x.type === 'advantage'}).sort((a,b) => (a.sort || 0) - (b.sort || 0));
        actorData.hasPT = (game.settings.get("andior-gurps", "useThresholdMagic") && (actorData.advantages.filter(x => x.name.startsWith("Magery")).length >= 1) );
    }

    /* ------------------------------------------------- */

    updateItems() {
        this.items.entries.forEach(item => {
            item.prepareData();
        });
    }

}