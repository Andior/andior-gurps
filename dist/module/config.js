export const AGURPS = {};

/** 
 * Possible skill base attributes
 * @type {Object}
 */
AGURPS.skillAttributes = {
    "st": "AGURPS.STShort",
    "dx": "AGURPS.DXShort",
    "iq": "AGURPS.IQShort",
    "ht": "AGURPS.HTShort",
    "wi": "AGURPS.WillShort",
    "pe": "AGURPS.PerShort",
    "10": "AGURPS.10Short"
};

AGURPS.skillDifficulty = {
    "e": "AGURPS.easyDifficultyShort",
    "a": "AGURPS.averageDifficultyShort",
    "h": "AGURPS.hardDifficultyShort",
    "v": "AGURPS.veryHardDifficultyShort",
}

AGURPS.skillDifficultyModifier = {
    "e": 0,
    "a": -1,
    "h": -2,
    "v": -3
}