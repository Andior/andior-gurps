/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class AGURPSItemSheet extends ItemSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["andior-gurps", "sheet", "item"],
            width: 520,
            height: 520,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
        });
    }

    get template() {
        const path = "systems/andior-gurps/templates/item";

        return `${path}/${this.item.data.type}-sheet.html`;
    }

    /** --------------------------------------------- */

    /** @override */

    getData() {
        const data = super.getData();
        data.dtypes = ["String", "Number", "Boolean"];
        data.config = CONFIG.AGURPS;

        return data;
    }

    /** --------------------------------------------- */

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.options.editable) return;

        // Roll handlers, click handlers, etc. would go here.

        html.find('.open-reference').click(this._onOpenReference.bind(this));
    }

    _onOpenReference(event) {
        event.preventDefault();
        const target = event.currentTarget;
        let ref;
        ref = $(target).siblings(".reference-input")[0].value;
        let [_, pdf, page] = ref.match(/([A-Z+])([0-9]+)/);
        page = parseInt(page);
        if (ui.PDFoundry) {
            ui.PDFoundry.openPDFByCode(pdf, { page });
        } else {
            ui.notifications.warn("PDFoundry must be installed and enabled to show references.")
        }
    }
}
