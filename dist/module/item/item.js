
import { convertPointsToModifier, promptForModifier } from '../util.js';

/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class AGURPSItem extends Item {
    /**
     * Augment the basic Item data model with additional dynamic data.
     */
    prepareData() {
        super.prepareData();
        // Get the Item's data
        const itemData = this.data;
        const actorData = this.actor ? this.actor.data : {};
        const data = itemData.data;

        data.openReference = () => {
            let ref = this.data.data.reference;
            let [_, pdf, page] = ref.match(/([A-Z+])([0-9]+)/);
            page = parseInt(page);
            if (ui.PDFoundry) {
                ui.PDFoundry.openPDFByCode(pdf, { page });
            } else {
                ui.notifications.warn("PDFoundry must be installed and enabled to show references.")
            }
        }

        if (itemData.type === 'skill') this._prepareSkillData(itemData);
        if (itemData.type === 'advantage') this._prepareAdvantageData(itemData);
    }

    /** --------------------------------------- */

    _prepareSkillData(itemData) {
        const actorData = this.actor ? this.actor.data : {};
        const data = itemData.data;

        let attribute = 10;
        if (data.attribute !== "10") {
            attribute = (actorData.data !== undefined) ? actorData.data.attributes[data.attribute].value : 10;
        }
        let difficulty = CONFIG.AGURPS.skillDifficultyModifier[data.difficulty];
        data.level = attribute + difficulty + convertPointsToModifier(data.points) + data.modifier;

        data.roll = (mod=0) => {
            let level = this.data.data.level + mod;
            let r = new Roll("3d6ms<=@level", {level: level});
            let [major, minor, patch] = game.data.version.split('.').map(x => parseInt(x));
            r.evaluate();
            r.toMessage({
                flavor: `${itemData.name} with effective skill ${level}`,
                speaker: ChatMessage.getSpeaker({actor: this.actor})
            });
        }

        data.rollWithMod = () => {
            promptForModifier().then(mod => {
                data.roll(mod)
            })
        }

        data.defaultRoll = data.roll;
    }

    _prepareAdvantageData(itemData) {
        const data = itemData.data;

        data.roll = () => {
            if ( this.data.data.control === null ) return;
            let r = new Roll("3d6ms<=@cr", {cr: this.data.data.control})
            r.evaluate();
            r.toMessage({
                flavor: `Self-control for ${this.data.name} with CR ${this.data.data.control}`,
                speaker: ChatMessage.getSpeaker({actor: this.actor})
            })
        }

        data.defaultRoll = data.roll;
    }
}
