export const preloadTemplates = async function () {
    const templatePaths = [
        // Add paths to "systems/andior-gurps/templates"
        "systems/andior-gurps/templates/actor/partials/attribute.html",
        "systems/andior-gurps/templates/actor/partials/resource.html"
    ];

    return loadTemplates(templatePaths);
}
