export const registerSettings = function() {
    // Register any custom system settings here
    game.settings.register("andior-gurps", "useThresholdMagic", {
        name: "Use Threshold Magic",
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    });
}
