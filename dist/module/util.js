
export function convertPointsToModifier(points) {
    if (points === 1) { return 0; }
    if (points >=  2 && points < 4) { return 1; }
    if (points >= 4 && points < 8) { return 2; }
    if (points >= 8) { return 3+Math.floor((points-8)/4); }
}

export const GURPSSkillRollRE = new RegExp("^\s*3d6ms<=?([0-9]+)\s*$", "i")

export function formatRollResult(formula, total) {
    if (GURPSSkillRollRE.test(formula)) {
        
        let level = parseInt(GURPSSkillRollRE.exec(formula)[1]);
        total = parseInt(total)
        let result = level - total;

        if (result <= 4 || (level >= 15 && result == 5) || (level >= 16 && result == 6)) {
            return `Critical success! ${result}, by ${total}`;
        }
        if (result == 18 || (level <= 15 && result == 17) || (result >= (level + 10))) {
            return `Critical failure! ${result}, by ${total}`;
        }
        if (total >= 0) {
            return `Success by ${total}`;
        }
        if (total < 0) {
            return `Failure by ${-total}`;
        }
    } else {
        return total;
    }
}

export async function promptForModifier() {
    return new Promise(resolve => {
        new Dialog({
        title: "Roll modifier",
        content: '<div class="modifier-prompt">Enter modifier: <input name="roll-modifier" type="text" value=0 data-dtype="Number"></div>',
        buttons: {
            confirm: {
                icon: '<i class="fas fa-check"></i>',
                label: "Roll!",
                callback: html => {resolve(parseInt(html[0].querySelector("input").value))}
            }
        }
        }, {}).render(true);
    });
}
